process.env.NODE_ENV = 'test';

var assert = require('assert')
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
chai.use(chaiHttp);

const connection = require('lib/db.js').getInstance()

describe('Test connection', function(){

  beforeEach(function(done){
    connection.query('DELETE FROM users where 1 = 1', function (error, results, fields) {
      done();
    });
  });
  afterEach(function(done){
    connection.query('DELETE FROM users where 1 = 1', function (error, results, fields) {
      done();
    });
  });

  it('Request with bad parameters', function(done) {
      chai.request(server)
          .post('/user/register')
          .send({})
          .end(function(err, res){
            res.body.should.have.property('res');
            res.body.should.have.property('message');
            res.body.res.should.equal('KO');
            res.body.message.should.equal('Bad parameters');
            done()
          });
  });

  it('Request with vulnerable password', function(done) {
    chai.request(server)
        .post('/user/register')
        .send({'first_name': 'Jonathan', 'last_name': 'Poirot', 'email': 'jonathan.poirot@hotmail.com', 'password': '1234'})
        .end(function(err, res){
          res.body.should.have.property('res');
          res.body.should.have.property('message');
          res.body.res.should.equal('KO');
          res.body.message.should.equal('This password is vulnerable');
          done()
        });
  });

  it('Regitred user', function(done) {
    chai.request(server)
        .post('/user/register')
        .send({'first_name': 'Jonathan', 'last_name': 'Poirot', 'email': 'jonathan.poirot@hotmail.com', 'password': '1108012341R'})
        .end(function(err, res){
          res.body.should.have.property('res');
          res.body.should.have.property('message');
          res.body.res.should.equal('OK');
          res.body.message.should.equal('user registered sucessfully');
          done()
        });
  });

})
