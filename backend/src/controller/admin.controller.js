﻿const express = require('express');
const router = express.Router();
const userService = require('./admin.service');
const authorize = require('_helpers/authorize')
const Role = require('_helpers/role');

// routes
router.get('/', getAll); // user only
module.exports = router;


function getAll(req, res, next) {
  userService.getAll(function (users) {
    users ? res.json(users) : res.status(400).json({ message: 'Bad request' })
  })
}

function setUser(req, res, next){
  userService.setUser(function (users) {
    users ? res.json(users) : res.status(400).json({ message: 'Bad request' })
  })
}
