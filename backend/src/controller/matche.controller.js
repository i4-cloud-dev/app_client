﻿const express = require('express');
const router = express.Router();
const authorize = require('_helpers/authorize');
const Role = require('_helpers/role');
const matcheService = require('../service/matche.service');

// routes
router.post('/', getMatches); // user only
router.post('/initialization',  authorize(Role.Arbitre), initialize); // user only
router.post('/round/incrementScore',  authorize(Role.Arbitre), increment); // user only
module.exports = router;


function getMatches(req, res, next){
  matcheService.getMatche(req.body)
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
        req.app.io.sockets.emit('user_register', {user: 'new connection'});
      })
}

function initialize(req, res, next){
  matcheService.initialize(req.body)
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
      })
}

function increment(req, res, next){
  matcheService.incrementScoreUser(req.body)
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
        req.app.io.sockets.emit('score_user_change', result);
      })
}

