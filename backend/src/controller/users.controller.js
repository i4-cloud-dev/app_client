﻿const express = require('express');
const router = express.Router();
const userService = require('../service/user.service');
const authorize = require('_helpers/authorize');
const Role = require('_helpers/role');
const User = require('../../models').User;


// routes
router.get('/', authorize(Role.Admin),getAll);
router.post('/authenticate', authenticate);
router.post('/authenticate/validation', validation_authenticate);
router.post('/2fa', verify2Fa);
router.post('/register', register);
router.post('/new', create);
router.get('/:id', authorize(Role.Admin), getById);       // all authenticated admin
module.exports = router;

function validation_authenticate(req, res, next) {
  userService.validate_authenticate(req.body)
      .then(user => {
        user ? res.json(user) : res.status(400).json({message: 'Username or password is incorrect'})
        req.app.io.sockets.emit('user_register', {user: 'new connection'});
      })
}


function getAll(req, res, next) {
  console.log(req.user.sub)
  var query = req.query

  User.findAll({
        where: query
  })
      .then( users => {
    users ? res.json(users) : res.status(400).json({OK: 'KO', message: 'No users'})
  })

}

function getById(req, res, next) {
  userService.getById(req.params.id, function (user) {
    user ? res.json(user) : res.status(400).json({message: 'No user with this ID'})
  })
}

function verify2Fa(req, res, next) {
  userService.verify2Fa(req.body)
      .then(response => {
        response ? res.json(response) : res.status(400).json({res: 'KO', message: 'The code does not match'})
      })
}

// async
function register(req, res, next) {
  var request = req.body
  userService.register(request)
      .then(user => {
        user ? res.json(user) : res.status(400).json({res: 'KO', message: 'The user is not create'})
      })
}

function authenticate(req, res, next) {
  userService.authenticate(req.body)
      .then(user => {
        user ? res.json(user) : res.status(400).json({message: 'Username or password is incorrect'})
        // req.app.io.sockets.emit('user_register', {user: 'new connection'});
      })
}

function create(req, res, next) {
  userService.create(req.body)
      .then(user => {
        user ? res.json(user) : res.status(400).json({message: 'Username or password is incorrect'})
      })
}

