﻿const express = require('express');
const router = express.Router();
const authorize = require('_helpers/authorize');
const Role = require('_helpers/role');
const tournoiService = require('../service/tournoi.service');

// routes
router.get('/', findAll); // user only
router.post('/new', authorize(Role.Admin), create); // user only
router.get('/arbitre',  authorize(Role.Arbitre), getTournament); // user only
router.post('/initialization',  authorize(Role.Arbitre), initialize); // user only
router.post('/matches', getMatchesOfTournament); // user only
module.exports = router;

function findAll(req, res, next) {
  tournoiService.findAll()
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
      })
}

function getTournament(req, res, next){
  tournoiService.find({user_id: req.user.sub})
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
      })
}

function getMatchesOfTournament(req, res, next){
  tournoiService.getMatcheOfTournament(req.body)
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
      })
}

function initialize(req, res, next){
  tournoiService.initialize(req.body)
      .then(result => {
        result ? res.json(result) : res.status(400).json({message: 'Bad request'})
      })
}

function create(req, res, next) {
  tournoiService.create(req.body)
      .then(response => {
        if (response) {
          res.json(response)
          req.app.io.sockets.emit('new_tournament', {tournament: response});
        } else {
          res.status(400).json({message: 'Bad request'})
        }

      })
}

