const connection = require('lib/db.js').getInstance()

module.exports = {
  creat,
  getAll,
  findBy,
  userExist
};

function creat(users, callBack){
  connection.query('INSERT INTO match SET ?',users, function (error, results, fields) {
    if (error) {
      callBack(error)
    }else{
      callBack(error, results);
    }
  });
}

function getAll(callback){
  connection.query('SELECT * FROM manche', function (error, results, fields){
    callback(error, results)
  })
}

function findBy(request, callback){
  var value = []
  var sql = ''

  Object.keys(request).forEach(function(key) {
    value.push(request[key]);
    sql += ' AND ' + key + ' = ? '
  });

  connection.query('SELECT * FROM users WHERE 1 = 1 ' + sql, value , function (error, results, fields){
    callback(error, results);
  })
}

function userExist(email, callback){
  connection.query('SELECT count(*) as user_exist FROM users WHERE email = ?', [email], function (error, results, fields){
    if(results)
      callback(error, results[0])
    callback(error, 0)
  })
}
