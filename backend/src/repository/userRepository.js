const connection = require('lib/db.js').getInstance()
const db = require('../../models');
const sequelize = require('sequelize')

module.exports = {
  creat,
  getAll,
  findBy,
  userExist,
  getToken,
  addConnection,
  validateConnexion,
  getConnexionsWithTime,
};

function creat(users, callBack){
  connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
    if (error) {
      callBack(error)
    }else{
      callBack(error, results["insertId"]);
    }
  });
}

function getAll(callback){
  connection.query('SELECT * FROM users', function (error, results, fields){
    callback(error, results)
  })
}

function findBy(request, callback){
  var value = []
  var sql = ''

  Object.keys(request).forEach(function(key) {
    value.push(request[key]);
    sql += ' AND ' + key + ' = ? '
  });

  connection.query('SELECT * FROM users WHERE 1 = 1 ' + sql, value , function (error, results, fields){
    callback(error, results);
  })
}

function userExist(email, callback){
  connection.query('SELECT count(*) as user_exist FROM users WHERE email = ?', [email], function (error, results, fields){
    if(results){
      callback(error, results[0])
    }else{
      callback(error, 0)
    }
  })
}

function getToken(user_mail, callback){
  connection.query('SELECT token FROM users WHERE email = ?', [user_mail], function (error, results, fields){
    callback(error, results[0].token)
  })
}


function addConnection({email, country, user_agent, user_ip, eu}, callback){
  findBy({email: email}, function (error, user) {
    if (!error){
      user = user[0];
      var user_id = user.id
      connection.query("INSERT INTO `connexion_client` (`id_connexion`, `user_id`, `date_connexion`, `user_agent`, `user_ip`, `success`, `eu`) VALUES (NULL, ?, CURRENT_TIMESTAMP, ?, ?, 0, ?);", [user_id, user_agent, user_ip, country, eu], function (error, results, fields){
        callback(results['insertId'])
      })
    } else {
      callback(error, {
        res: 'KO',
        message: 'The connection system is temporarily unavailable'
      })
    }
  })
}

async function validateConnexion({id, validation}){
  connection.query("UPDATE `connexion_client` SET success = ? WHERE id_connexion = ?", [validation, id], function (error, results, fields){})
}


// async function getConnexionsWithTime(email){
//   var return_result = false;
//   await findBy({email: email}, function (error, user) {
//     console.log(error)
//     if (!error){
//       user = user[0];
//       var user_id = user.id
//       connection.query("SELECT count(*) as NB_CONNEXION FROM `connexions` WHERE `date_connexion` >= NOW() - INTERVAL 1 DAY AND user_id = ?", [user_id], function (error, results, fields){
//         console.log(results[0]['NB_CONNEXION'] <= 300)
//         return_result = (results[0]['NB_CONNEXION'] <= 300);
//       })
//     }
//   });
//   return return_result;
// }

async function getConnexionsWithTime(user_id){
  var return_result = false;
  await db.sequelize.query('SELECT count(*) as NB_CONNEXION FROM `Connexions` WHERE `date_connexion` >= NOW() - INTERVAL 1 DAY AND userId = :id ',
      { replacements: { id: user_id }, type: db.sequelize.QueryTypes.SELECT }
  ).then(nb_connexion => {
    return_result = (nb_connexion[0]['NB_CONNEXION'] < 300)
  })
  return return_result;
}
