const connection = require('lib/db.js').getInstance()

module.exports = {
  creat,
  getAll,
  findBy,
  userExist
};

function creat(users, callBack){
  connection.query('INSERT INTO admin SET ?',users, function (error, results, fields) {
    if (error) {
      callBack(error)
    }else{
      callBack(error, results);
    }
  });
}

function getAll(callback){
  connection.query('SELECT * FROM admin', function (error, results, fields){
    callback(error, results)
  })
}

function findBy(request, callback){
  var value = []
  var sql = ''

  Object.keys(request).forEach(function(key) {
    value.push(request[key]);
    sql += ' AND ' + key + ' = ? '
  });

  connection.query('SELECT * FROM admin WHERE 1 = 1 ' + sql, value , function (error, results, fields){
    callback(error, results);
  })
}

function userExist(email, callback){
  connection.query('SELECT count(*) as user_exist FROM admin WHERE email = ?', [email], function (error, results, fields){
    callback(error, results[0])
  })
}
