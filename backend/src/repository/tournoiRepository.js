const connection = require('lib/db.js').getInstance();
const sequelize = require('sequelize');

module.exports = {
  getTournament
};


async function getTournament(user_id){
  var return_result = false;
  await db.sequelize.query('SELECT * FROM `Connexions` WHERE `date_connexion` >= NOW() - INTERVAL 1 DAY AND userId = :id ',
      { replacements: { id: user_id }, type: db.sequelize.QueryTypes.SELECT }
  ).then(nb_connexion => {
    return_result = (nb_connexion[0]['NB_CONNEXION'] < 300)
  })
  return return_result;
}
