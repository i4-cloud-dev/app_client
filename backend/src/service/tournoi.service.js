﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const Role = require('_helpers/role');
const crypto = require('crypto');
const tournoiRepository = require('../repository/tournoiRepository')

const db = require('../../models');
const Op = db.Sequelize.Op;
const Tournament = require('../../models').Tournoi;
const User = require('../../models').User;
const Matche = require('../../models').Matche;
const Matche_user = require('../../models').MatcheUser;
const Tournament_user = require('../../models').TournamentUser;
const Tournament_arbitre = require('../../models').TournamentArbitre;

module.exports = {
  create,
  findAll,
  find,
  initialize,
  getMatcheOfTournament
};

async function findAll() {
  var return_value = null;

  await Tournament.findAll().then(tournaments => {
    if (tournaments) return_value = tournaments
  })

  return return_value;
}
async function find({user_id}) {
  var return_value = null;
  var request = {};

  if (user_id !== undefined || user_id !== "") {
    request = {
      include: [{
        model: User,
        as: 'Tournament_arbitre',
        paranoid: false,
        required: false,
        where: {id: user_id}
      }],
      raw: true
    }
  }

  await Tournament.findAll(request)
      .then(tournaments => {
        if (tournaments) return_value = tournaments
      })
      .catch(error => {
        console.log(error)
      })

  return return_value;
}

async function create({nom, date_debut, description, users, arbitres}) {
  var return_value = null;
  var tournament = null;
  var tournoi = {
    'nom': nom,
    'date_debut': new Date(date_debut),
    'description': description,
  };

  await Tournament.create(tournoi)
      .then(result => {
        tournament = result
        return_value = {
          OK: 'OK',
          user: result.get({plain: true})
        }
      }).catch(error => {
        console.log(error)
      })

  users.forEach(function (user) {
    Tournament_user.create({
      tournament_id: tournament.get('id'),
      user_id: user
    })
  });
  arbitres.forEach(function (arbitre) {
    Tournament_arbitre.create({
      tournament_id: tournament.get('id'),
      arbitre_id: arbitre
    })
  });

  return return_value;
}


async function initialize({tournament_id}) {
  var return_value = null;
  var request = {};
  var tournamant = null;
  var player = null;

  if (tournament_id !== undefined || tournament_id !== "") {
    request = {
      include: [{
        model: User,
        as: 'Tournament_user',
      }],
      where: {id: tournament_id}
    }
  }else {
    return {
      OK: 'KO',
      message: 'bad parameters'
    }
  }

  await Tournament.findOne(request)
      .then(response => {
        if (response === null) return_value = {res: 'KO', message: 'Tournament probably not exist'}
        else tournamant = response
      })
      .catch(error => {
        console.log(error)
      })

  if (return_value != null) return return_value;
  var tournament_plain_text = tournamant.get({ plain: true });

  if(tournamant.get('started') !== 1){
    await creatTournament(tournamant, tournament_plain_text.Tournament_user);
    tournamant.update({started: true});
  }

  await getMatcheTournoiCurent(tournamant)
      .then(response => {
        return_value = response
        console.log(return_value)
      })
      .catch(error => {
        console.log(error)
      })

  return return_value;
}

async function getMatcheTournoiCurent(tournament){
  var tournament_result = null;

  await Tournament.findOne({
    attributes: ['id'],
    include: [{
      model: Matche,
      include: [{
        model: User,
        as: 'Matche_user',
      }],
      where: {
        // start_time: {[Op.eq]: null}
      }
    }],
    where: {id: tournament.get('id')}
  }).then(result => {
    tournament_result = (result !== null) ? result.get({plain: true}) : {OK: 'OK', Matches : {}}
  }).catch(error => {
    console.log(error)
  })

  return tournament_result
}

async function creatTournament(tournament, usersTournament){
  var col1 = calculeColumn(usersTournament.length).col1
  var matche = null;
  for (var i = 0; i < col1; i += 2){
    matche = null;
    console.log(usersTournament[i].id + ' - ' + usersTournament[i + 1].id)
    await Matche.create({tournoiId: tournament.get('id')})
      .then(result => {
        matche = result
      }).catch(error => {
        console.log(error)
      })

    for(var j = 0; j < 2; j++){
      await Matche_user.create({
            matche_id: matche.get('id'),
            user_id: usersTournament[i + j].id
          })
          .then()
          .catch(error => {
            console.log(error)
          })
    }
  }
  console.log('finish - ' + tournament.get('id'))
  return {OK: 'OK'}
}

function calculeColumn(nbPlayer) {
  let rang = 2
  let col1 = 1
  let col2 = 0
  if (nbPlayer <= 1) {
    return null
  }

  while (nbPlayer > rang) {
    rang = rang + rang
  }

  col2 = rang - nbPlayer
  col1 = nbPlayer - col2
  return {col1, col2}
}


async function getMatcheOfTournament({tournament_id}){
  var return_value = null;
  var request = {};
  var tournamant = null;

  if (tournament_id !== undefined && tournament_id !== "") {
    request = {
      include: [{
        model: User,
        as: 'Tournament_user',
      }],
      where: {id: tournament_id}
    }
  }else {
    return {
      OK: 'KO',
      message: 'bad parameters'
    }
  }

  await Tournament.findOne(request)
      .then(response => {
        if (response === null) return_value = {res: 'KO', message: 'Tournament probably not exist'}
        else tournamant = response
      })
      .catch(error => {
        console.log(error)
      })

  if (return_value != null) return return_value;

  await getMatcheTournoiCurent(tournamant)
      .then(response => {
        return_value = response
        console.log(return_value)
      })
      .catch(error => {
        console.log(error)
      })

  return return_value;
}
