﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const Role = require('_helpers/role');
const crypto = require('crypto');
const adminRepository = require('../repository/adminRepository')

module.exports = {
  authenticate,
  register,
  getAll,
  getById
};

function authenticate({email, password}, callBack) {
  adminRepository.findBy({email: email}, function (error, user) {
    if (!error){
      password = crypto.createHash('sha256').update(password).digest('hex')
      user = user[0];
      if (user && user.password === password) {
        const token = jwt.sign({ sub: user.id, role: user.role }, config.secret)
        const {password, ...userWithoutPassword} = user;
        callBack({
          ...userWithoutPassword,
          token
        });
      }else{
        callBack()
      }
    } else {
      callBack()
    }
  });
}

function register({ first_name, last_name, email, password}, callBack) {
  let hash = crypto.createHash('sha256');
  var today = new Date();
  var users={
    "first_name":first_name,
    "last_name":last_name,
    "email":email,
    "password": crypto.createHash('sha256').update(password).digest('hex'),
    "created":today,
    "modified":today,
    "role": "User"
  };

  adminRepository.userExist(email, function (error, userCount) {
    if(!error && userCount.user_exist == 0){
      adminRepository.creat(users, function (error, res) {
        if (error) {
          callBack({
            code: 400,
            failed: "error ocurred"
          })
        }else {
          callBack({
            code: 200,
            success: "user registered sucessfully",
            id: res
          })
        }
      })
    }else if (userCount > 0){
      callBack({failed: 'This user alrady exist'})
    }else {
      callBack({failed: 'Request error'})
    }
  })
}


function getAll(callBack) {
  adminRepository.getAll(function (error, users) {

    if (error) {
      callBack({
        code: 400,
        failed: "error ocurred"
      })
    }else {
      if (users) {
        const {password, ...userWithoutPassword} = users;
        callBack(userWithoutPassword)
      }
    }

  })
}

function getById(id, callBack) {
  adminRepository.findBy({id: parseInt(id)}, function (error, user) {
    console.log(user)
    user = user[0];
    if (!user) return;
    const {password, ...userWithoutPassword} = user;
    callBack(userWithoutPassword);
  });
}
