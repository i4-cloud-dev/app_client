const {pwnedPassword} = require('hibp');
const config = require('config.json');
const jwt = require('jsonwebtoken');
const Role = require('_helpers/role');
const Sequelize = require('sequelize');
var speakeasy = require('speakeasy');
const crypto = require('crypto');
const userRepository = require('../repository/userRepository')
const userService = require('./user.service');
var geoip = require('geoip-lite');

const db = require('../../models');
const User = require('../../models').User;
const error_message = require('../../_helpers/error_message');
const Connection = require('../../models').Connexion;

module.exports = {
  authenticate,
  register,
  verify2Fa,
  validate_authenticate,
  create
}

async function create({first_name, last_name, email, role}){
  var return_value = null
  var user_info = {
    first_name,
    last_name,
    email,
    role
  }
  await User.create(user_info)
      .then(user => {
        return_value = {
          OK: 'OK',
          message: 'User is succefully crerate',
          user: {
            id: user.id
          }
        }
      }).catch(err => {
        return_value = {
          OK: 'KO',
          message: error_message.user(err.original.code)
        }
      })
  return return_value
}

function generate2FA() {
  var secret = speakeasy.generateSecret({length: 20});
  secret.otpauth_url = secret.otpauth_url.replace('SecretKey', 'ping-pong')
  return secret
}

async function verify2Fa({email, code_validation}) {
  var return_value = null;
  await User.findOne({ attributes: ['token'], where: {email: email}})
      .then(result => {
          var verified = speakeasy.totp.verify({
            secret: result.get('token'),
            encoding: 'base32',
            token: code_validation
          });

          if (verified) {
            return_value = {
              res: 'OK',
              message: "Good code"
            }
          } else {
            return_value = {
              code: 400,
              res: 'KO',
              message: "Bad code"
            }
          }
      })

  return return_value;
}


async function ifAllowConnexion(user) {
  var isAllow = true;
  var message = [];
  await userRepository.getConnexionsWithTime(user.get('id'))
      .then(result => {
        if (isAllow && result === false) {
          isAllow = result
          message.push('Too much connection please try again in a few hours')
        }
      });

  await Connection.findAll({
    attributes: [
      [db.sequelize.fn('COUNT', db.sequelize.col('user_ip')), 'NB_USERIP'],
      'user_ip'
    ],
    where: {
      userId: user.get('id')
    },
    group: ['user_ip']
  }).then(result => {
    result.forEach(function (element) {
      var nb_element = element.get('NB_USERIP')
      var ip_element = element.get('user_ip')

      if (ip_element === user.get('user_ip')) {
        if (((nb_element <= 3 && element.length === undefined) || (nb_element <= 3 && element.length < 3)) || nb_element > 3) {

        } else {
          isAllow = false
          message.push('Your IP is not allowed')
        }

      }
    })
  });


  await Connection.findAll({
    attributes: [
      [db.sequelize.fn('COUNT', db.sequelize.col('user_agent')), 'NB_USER_AGENT'],
      'user_agent'
    ],
    where: {
      userId: user.get('id')
    },
    group: ['user_agent']
  }).then(result => {
    result.forEach(function (element) {
      var nb_element = element.get('NB_USER_AGENT')
      var agent_element = element.get('user_agent')
      if (agent_element === user.get('user_agent')) {
        if (((nb_element <= 3 && element.length === undefined) || (nb_element <= 3 && element.length < 3)) || nb_element > 3) {

        } else {
          isAllow = false
          message.push('This browser is not allowed')
        }
      }
    })
  });

  return {
    isAllow: isAllow,
    messages: message
  }
}

async function register({first_name, last_name, email, password}) {
  if (first_name === undefined || last_name === undefined || email === undefined || password === undefined) {
    return ({
      res: 'KO',
      failed: "Bad parameters",
      message: 'Bad parameters'
    })
  } else {
    var return_value = null;
    await User.findOne({where: {email: email}})
        .then(response => {
          if (response !== null) {
            return_value = {res: 'KO', message: 'This user alrady exist'}
          }
        })

    if (return_value != null) return return_value;

    await pwnedPassword(password)
        .then(numPwns => {
          if (numPwns !== 0) {
            return_value = {
              res: 'KO',
              message: "This password is vulnerable"
            }
          }
        })

    if (return_value != null) return return_value;

    var tokenTemp = generate2FA();
    var token = tokenTemp.base32
    var user_info = {
      "first_name": first_name,
      "last_name": last_name,
      "email": email,
      "password": crypto.createHash('sha256').update(password).digest('hex'),
      "role": "User",
      "token": token
    };

    await User.create(user_info)
        .then(user => {
          return_value = {
            OK: 'OK',
            message: 'Your succesfully registred',
            user: {
              id: user.id,
              token: tokenTemp
            }
          }
        }).catch(err => {
          return_value = {
            OK: 'KO',
            message: 'error'
          }
        })

    return return_value;
  }
}

async function authenticate({email, password, user_agent, user_ip}) {
  var return_result = null
  var user = null
  var connexion = null
  var geo = geoip.lookup(user_ip)

  if (!geo)
    return {
      res: 'KO',
      message: 'Your ip is not correct'
    }

  await User.findOne({where: {email: email}})
      .then(response => {
        if (response === null) return_result = {res: 'KO', message: 'Username or password is incorrect'}
        else user = response
      })
  if (return_result != null) return return_result;

  user.createConnexion({
    user_agent: user_agent,
    user_ip: user_ip,
    eu: geo.eu,
    success: false
  }).then(result => {
    if (result) connexion = result
    else return_result = {res: 'KO', message: 'Error occured'}
  })

  await ifAllowConnexion(user)
      .then(is_allow => {
        if (is_allow.isAllow) {
          if (!geo.eu) {
            return_result = {
              res: 'KO',
              message: 'The connection system is not allowed in your country'
            }
          }
        } else {
          return_result = {
            res: 'KO',
            message: 'Too much connection please try again in a few hours'
          }
        }
      })
  if (return_result != null) return return_result;

  password = crypto.createHash('sha256').update(password).digest('hex')
  if (user.password === password) {
    connexion.update({success: 1})
    return {
      res: 'OK'
    }
  } else {
    return {
      res: 'KO',
      message: 'Username or password is incorrect'
    }
  }
}

async function validate_authenticate({email, password, user_agent, user_ip, code_validation}) {
  var return_result = null;
  var user = null;

  await User.findOne({where: {email: email}})
      .then(response => {
        if (response === null) return_result = {res: 'KO', message: 'Username or password is incorrect'}
        else user = response
      })
  if (return_result != null) return return_result;

  password = crypto.createHash('sha256').update(password).digest('hex')

  if (user.get('password') === password) {
    await verify2Fa({email: user.get('email'), code_validation})
        .then(result => {
          const token = jwt.sign({sub: user.get('id'), role: user.get('role')}, config.secret)
          console.log(config.secret)
          const {password, ...userWithoutPassword} = user.get({ plain: true });
          return_result = {
            res: 'OK',
            ...userWithoutPassword,
            token,
            message: 'Your connected'
          }
        });
  } else {
    return_result = {res: 'KO', message: 'Username or password is incorrect'}
  }

  console.log(return_result);
  return return_result;
}
