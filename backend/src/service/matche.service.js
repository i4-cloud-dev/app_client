﻿const config = require('config.json');
const Role = require('_helpers/role');
const crypto = require('crypto');
const mancheRepository = require('../repository/mancheRepository')

const db = require('../../models');
const Op = db.Sequelize.Op;
const Round = require('../../models').Round;
const User = require('../../models').User;
const Matche = require('../../models').Matche;
const Matche_user = require('../../models').MatcheUser;
const Score_round_user = require('../../models').ScoreRoundUser;

module.exports = {
  initialize,
  incrementScoreUser,
  getMatche
};

async function initialize({matche_id}) {
  var return_value = null;
  var request = {};
  var matche = null;
  var player = null;

  if (matche_id !== undefined || matche_id !== "") {
    request = {
      include: [
        {
          model: User,
          as: 'Matche_user',
          attributes: ['id']
        }
      ],
      where: {id: matche_id}
    }
  } else {
    return {
      OK: 'KO',
      message: 'bad parameters'
    }
  }

  await Matche.findOne(request)
      .then(response => {
        if (response === null) return_value = {res: 'KO', message: 'Matche probably not exist'}
        else matche = response
      })
      .catch(error => {
        console.log(error)
      })

  if (return_value != null) return return_value;

  if (matche.get('start_time') === null) {
    matche.update({start_time: new Date()});
    await creatRound(matche)
        .then(response => {
          return_value = response
        })
        .catch(error => {
          console.log(error);
        })
  }

  return {OK: 'OK'};
}

async function creatRound(matche) {
  var round = null;

  await Round.create({matcheId: matche.get('id')})
      .then(response => {
        round = response
      })

  var usersRound = matche.get('Matche_user')

  for (var i = 0, len = usersRound.length; i < len; i++) {
    await Score_round_user.create({
      round_id: round.get('id'),
      user_id: usersRound[i].get('id')
    })
        .then(result => {
          console.log(result)
        })
        .catch(error => {
          console.log(error)
        })
  }
}


async function incrementScoreUser({user_id, round_id}){
  var return_value = null;
  var request = {};
  var round = null;

  if (user_id !== undefined && user_id !== "" && round_id !== undefined && round_id !== "") {
    request = {
      where: {id: round_id},
      include: [
        {
          model: User,
          as: 'round_user',
          attributes: ['id', 'first_name'],
          // where : {id: user_id}
        }
      ]
    }
    // request = {
    //   where: {user_id: user_id, round_id: round_id},
    // }
  } else {
    return {
      OK: 'KO',
      message: 'bad parameters'
    }
  }

   await Round.findOne(request)
      .then(response => {
        if (response === null) return_value = {res: 'KO', message: 'Matche probably not exist'}
        else round = response
      })
      .catch(error => {
        console.log(error)
      })

  var round_user = round.get('round_user')
  var win_user =  round_user.filter(user => user['id'] === user_id)
  var other_user =  round_user.filter(user => user['id'] !== user_id)

  var score_user = win_user[0].get('ScoreRoundUser').get('score')
  score_user ++
  await win_user[0].get('ScoreRoundUser').update({score: score_user ++ })


  if (score_user - other_user[0].get('ScoreRoundUser') > 2 && score_user >= 11){

  }


  var matche = {OK: 'KO'}
  await getMatche({matche_id: round.get('matcheId')})
      .then(response => {
        matche = response
      })
      .catch( response => {
        console.log(response)
      })

  return matche
}


async function getMatche({matche_id}) {
  var return_value = null;
  var request = {};
  var matche = null;

  if (matche_id !== undefined || matche_id !== "") {
    request = {
      attributes: ['id'],
      include: [
        {
          model: Round,
          include: [
            {
              model: User,
              as: 'round_user',
              attributes: ['id', 'first_name']
            }
          ],
          attributes: ['id', 'start_time']
        },
      ],
      where: {id: matche_id}
    }
  } else {
    return {
      OK: 'KO',
      message: 'bad parameters'
    }
  }

  await Matche.findOne(request)
      .then(response => {
        if (response === null) return_value = {res: 'KO', message: 'Matche probably not exist'}
        else matche = response
      })
      .catch(error => {
        console.log(error)
      })

  return matche.get({plain: true});
}
