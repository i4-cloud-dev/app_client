'use strict';
module.exports = (sequelize, DataTypes) => {
  const MatcheUser = sequelize.define('MatcheUser', {
    matche_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
  }, {});
  MatcheUser.associate = function(models) {
    // associations can be defined here
  };
  return MatcheUser;
};