'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tournoi = sequelize.define('Tournoi', {
    nom: DataTypes.STRING,
    date_debut: DataTypes.DATE,
    date_fin: DataTypes.DATE,
    description: DataTypes.STRING,
    started: DataTypes.STRING
  }, {});

  Tournoi.associate = function(models) {
    Tournoi.hasMany(models.Matche)

    Tournoi.belongsToMany(models.User, {
      through: 'TournamentArbitres',
      as: 'Tournament_arbitre',
      foreignKey: 'tournament_id'
    })

    Tournoi.belongsToMany(models.User, {
      through: 'TournamentUsers',
      as: 'Tournament_user',
      foreignKey: 'tournament_id'
    })
  };
  return Tournoi;
};
