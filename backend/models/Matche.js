'use strict';
module.exports = (sequelize, DataTypes) => {
  const Matche = sequelize.define('Matche', {
    tournoiId: DataTypes.INTEGER,
    start_time: DataTypes.DATE,
    end_time: DataTypes.DATE,
    finished: DataTypes.BOOLEAN
  }, {});
  Matche.associate = function(models) {
    Matche.belongsTo(models.Tournoi)

    Matche.hasMany(models.Round)

    Matche.belongsToMany(models.User, {
      through: 'MatcheUsers',
      as: 'Matche_user',
      foreignKey: 'matche_id'
    })

    Matche.belongsToMany(models.Round, {
      through: 'ScoreRoundUsers',
      as: 'Score_user',
      foreignKey: 'matche_id'
    })
  };
  return Matche;
};
