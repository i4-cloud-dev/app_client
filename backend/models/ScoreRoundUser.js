'use strict';
module.exports = (sequelize, DataTypes) => {
  const ScoreRoundUser = sequelize.define('ScoreRoundUser', {
    score: DataTypes.INTEGER,
    round_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
  }, {});
  ScoreRoundUser.associate = function(models) {
    // associations can be defined here
  };
  return ScoreRoundUser;
};
