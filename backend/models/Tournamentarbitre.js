'use strict';
module.exports = (sequelize, DataTypes) => {
  const TournamentArbitre = sequelize.define('TournamentArbitre', {
    tournament_id: DataTypes.INTEGER,
    arbitre_id: DataTypes.INTEGER
  }, {});
  TournamentArbitre.associate = function(models) {
    // associations can be defined here
  };
  return TournamentArbitre;
};
