'use strict';
module.exports = (sequelize, DataTypes) => {
  const Round = sequelize.define('Round', {
    matcheId: DataTypes.INTEGER,
    start_time: DataTypes.DATE,
    end_time: DataTypes.TIME
  }, {});
  Round.associate = function(models) {
    Round.belongsTo(models.Matche)

    Round.belongsToMany(models.User, {
      through: 'ScoreRoundUser',
      as: 'round_user',
      foreignKey: 'round_id'
    })
  };
  return Round;
};
