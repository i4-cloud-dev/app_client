'use strict';
module.exports = (sequelize, DataTypes) => {
  const Connexion = sequelize.define('Connexion', {
    date_connexion: DataTypes.DATE,
    user_agent: DataTypes.STRING,
    user_ip: DataTypes.STRING,
    success: DataTypes.BOOLEAN,
    eu: DataTypes.STRING
  }, {});
  Connexion.associate = function(models) {
    Connexion.belongsTo(models.User)
  };
  return Connexion;
};
