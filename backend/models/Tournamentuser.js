'use strict';
module.exports = (sequelize, DataTypes) => {
  const TournamentUser = sequelize.define('TournamentUser', {
    tournament_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
  }, {});
  TournamentUser.associate = function(models) {
    // associations can be defined here
  };
  return TournamentUser;
};
