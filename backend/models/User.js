'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
    token: DataTypes.STRING,
    blocked: DataTypes.BOOLEAN
  }, {});
  User.associate = function(models) {
    User.hasMany(models.Connexion)

    User.belongsToMany(models.Tournoi, {
      through: 'TournamentUsers',
      as: 'tournament_user',
      foreignKey: 'user_id'
    })

    User.belongsToMany(models.Tournoi, {
      through: 'TournamentArbitres',
      as: 'tournament_arbitre',
      foreignKey: 'arbitre_id'
    })

    User.belongsToMany(models.Matche, {
      through: 'MatcheUsers',
      as: 'Matche_user',
      foreignKey: 'user_id'
    })

    User.belongsToMany(models.Round, {
      through: 'ScoreRoundUser',
      as: 'round_user',
      foreignKey: 'user_id'
    })
  };
  return User;
};
