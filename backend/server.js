﻿require('rootpath')();

var http = require('http');
const express = require('express.io');
const app = express();
const bodyParser = require('body-parser');
const {RateLimiterMemory} = require('rate-limiter-flexible');
const cors = require('cors');

const rateLimiter = new RateLimiterMemory({
  points: 5, // 5 points
  duration: 1, // per second
});

app.use(function (req, res, next) {
  rateLimiter.consume(req.connection.remoteAddress) // consume 1 point per event
      .then(() => {
        next();
      })
      .catch((rejRes) => {
        res.status(400).json({error: ''})
      });
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

// api routes
// app.use('/admin', require('./role/admin/admin.controller'));
app.use('/user', require('./src/controller/users.controller'));
app.use('/tournament', require('./src/controller/tournoi.controller'));
app.use('/matche', require('./src/controller/matche.controller'));

// start server
app.use(cors({credentials: true, origin: 'http://localhost:8080'}));
const port = process.env.NODE_ENV === 'production' ? 80 : 3000;
const server = app.listen(port, '127.0.0.1', function () {
  console.log('Server listening on port ' + port);
});

const io = require('socket.io')(server)
io.on('connection', function(socket) {
  console.log(socket.id)
});

io.origins('localhost:* ');
app.io = io

module.exports = app;
