'use strict'

const mysql = require('mysql');
const config = require('config.json');
var instance;

module.exports = {
  getInstance,
  setInstance
};

function createInstance(config) {
  var connection = mysql.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
    port: config.port
  })

  connection.connect(function (err) {
    if (!err) {
      console.log("Database is connected ...");
    } else {
      console.log(err+"Error connecting database ...");
    }
  })

  return connection;
}

function getInstance() {
  if (!instance) {
    instance = createInstance(config);
  }
  return instance;
}

function setInstance(config) {
  instance = createInstance(config);
}

