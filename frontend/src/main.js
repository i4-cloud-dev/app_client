// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueResource from 'vue-resource'
import VueSession from 'vue-session'
import VueClipboard from 'vue-clipboard2'
import {store} from './components/store'
import {Auth} from './mixin/connection'
import VueFilterDateFormat from 'vue-filter-date-format'
import VeeValidate from 'vee-validate'
import './assets.js'

Vue.use(VueClipboard)
Vue.use(VueResource)
Vue.use(VueSession)
Vue.use(require('vue-moment'))
Vue.use(VeeValidate)

Vue.use(VueFilterDateFormat, {
  monthNames: [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ],
  monthNamesShort: [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ]
})

Vue.mixin(Auth)

Vue.config.productionTip = false
axios.defaults.withCredentials = true
VueClipboard.config.autoSetContainer = true // add this line

Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('jwt')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  mixin: [Auth],
  components: { App },
  template: '<App/>'
})
