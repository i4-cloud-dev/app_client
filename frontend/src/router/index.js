import Vue from 'vue'
import Router from 'vue-router'
import {store} from '../components/store'

import empty from '@/components/divers/index.vue'

import index from '@/components/index.vue'
import home from '@/components/index/home.vue'
import matche from '@/components/divers/matche/matche.vue'
import tournaments from '@/components/index/tournaments.vue'

// -------------- user --------------------
import homeAdmin from '@/components/admin/home.vue'
import indexAdmin from '@/components/admin/index.vue'
// --- user ---
import userAdmin from '@/components/admin/users.vue'
import addUserAdmin from '@/components/divers/user/add.vue'
// --- tournament ---
import tournamentAdmin from '@/components/admin/tournament.vue'
import addTournamentAdmin from '@/components/divers/tournament/add.vue'

import homeArbitre from '@/components/arbitre/home.vue'
import indexArbitre from '@/components/arbitre/index.vue'
import matcheArbitre from '@/components/divers/matche/matche-arbitre.vue'

import connection from '@/components/connection/sing_in.vue'
import inscription from '@/components/connection/sing_up.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/admin',
      name: 'admin',
      redirect: '/admin',
      component: indexAdmin,
      meta: {
        requiresAuth: true,
        is_admin: true
      },
      children: [
        {
          path: '/',
          name: 'homeAdmin',
          component: homeAdmin
        },
        {
          // ---------------- User ----------------
          path: '/admin/user',
          name: 'indexUserAdmin',
          component: empty,
          children: [
            {
              path: '/admin/user',
              name: 'userAdmin',
              component: userAdmin
            },
            {
              path: '/admin/user/new',
              name: 'addUserAdmin',
              component: addUserAdmin
            },
            {
              path: '*',
              redirect: '/admin/user'
            }
          ]
        },
        {
          // ---------------- tournament ----------------
          path: '/admin/tournaments',
          name: 'indexTournamenAdmin',
          component: empty,
          children: [
            {
              path: '/admin/tournaments',
              name: 'tournamentAdmin',
              component: tournamentAdmin
            },
            {
              path: '/admin/tournaments/new',
              name: 'addTournamentAdmin',
              component: addTournamentAdmin
            },
            {
              path: '*',
              redirect: '/admin/tournament'
            }
          ]
        },
        {
          path: '*',
          redirect: '/admin'
        }
      ]
    },
    {
      path: '/arbitre',
      name: 'arbitre',
      redirect: '/arbitre',
      component: indexArbitre,
      meta: {
        requiresAuth: true,
        is_arbitre: true
      },
      children: [
        {
          path: '/',
          name: 'homeArbitre',
          component: homeArbitre
        },
        {
          name: 'matcheArbitre',
          path: '/arbitre/matche/:id',
          component: matcheArbitre
        },
        {
          path: '*',
          redirect: '/arbitre'
        }
      ]
    },
    {
      path: '',
      component: index,
      children: [
        {
          path: '/',
          name: 'home',
          component: home
        },
        {
          name: 'matche',
          path: '/matche/:id',
          component: matche
        },
        {
          name: 'connection',
          path: '/connection',
          component: connection
        },
        {
          path: '/inscription',
          name: 'inscription',
          component: inscription
        },
        {
          path: '/tournaments',
          name: 'tournaments',
          component: tournaments
        },
        {
          path: '*',
          redirect: '/'
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/connection',
        params: {nextUrl: to.fullPath}
      })
    } else {
      let user = JSON.parse(localStorage.getItem('user'))
      if (to.matched.some(record => record.meta.is_admin)) {
        if (user.role === store.state.get_role.admin) {
          next()
        } else {
          next({name: 'home'})
        }
      } else if (to.matched.some(record => record.meta.is_arbitre)) {
        if (user.role === store.state.get_role.arbitre) {
          next()
        } else {
          next({name: 'home'})
        }
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('jwt') == null) {
      next()
    } else {
      next({name: 'home'})
    }
  } else {
    next()
  }
})

export default router
