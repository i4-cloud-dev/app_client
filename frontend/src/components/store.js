import Vue from 'vue'
import Vuex from 'vuex'
import user from '../lib/user'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    get_role: user.role,
    is_connected: user.is_connected(),
    is_admin: user.is_admin(),
    is_arbitre: user.is_arbitre(),
    is_user: user.is_user()
  }
})
