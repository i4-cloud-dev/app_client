
export const Auth = {
  methods: {
    Disconnected () {
      this.$store.state.is_connected = false
      this.$store.state.is_admin = false
      this.$store.state.is_arbitre = false
      this.$store.state.is_user = false
      localStorage.clear()
      this.$router.push('home')
    }
  }
}
