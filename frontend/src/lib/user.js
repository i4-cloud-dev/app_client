exports.role = {
  'admin': 'Admin',
  'arbitre': 'Arbitre',
  'user': 'User'
}

exports.is_connected = function () {
  return (localStorage.getItem('jwt') !== null)
}

exports.is_admin = function () {
  var userStorage = localStorage.getItem('user')
  userStorage = (userStorage !== undefined && userStorage != null) ? JSON.parse(userStorage) : null
  return (userStorage !== null && userStorage.role === this.role.admin)
}

exports.is_arbitre = function () {
  var userStorage = localStorage.getItem('user')
  userStorage = (userStorage !== undefined && userStorage != null) ? JSON.parse(userStorage) : null
  return (userStorage !== null && userStorage.role === this.role.arbitre)
}

exports.is_user = function () {
  var userStorage = localStorage.getItem('user')
  userStorage = (userStorage !== undefined && userStorage != null) ? JSON.parse(userStorage) : null
  return (userStorage !== null && userStorage.role === this.role.user)
}
