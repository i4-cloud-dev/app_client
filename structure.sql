#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: JOUEUR
#------------------------------------------------------------

CREATE TABLE JOUEUR(
        ID_JOUEUR Int  Auto_increment  NOT NULL ,
        NOM       Varchar (50) NOT NULL ,
        PRENOM    Varchar (50) NOT NULL
	,CONSTRAINT JOUEUR_PK PRIMARY KEY (ID_JOUEUR)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ARBITRE
#------------------------------------------------------------

CREATE TABLE ARBITRE(
        ID_ARBITRE Int  Auto_increment  NOT NULL ,
        NOM        Varchar (50) NOT NULL
	,CONSTRAINT ARBITRE_PK PRIMARY KEY (ID_ARBITRE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: TOURNOI
#------------------------------------------------------------

CREATE TABLE TOURNOI(
        ID_TOURNOI Int  Auto_increment  NOT NULL
	,CONSTRAINT TOURNOI_PK PRIMARY KEY (ID_TOURNOI)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MANCHE
#------------------------------------------------------------

CREATE TABLE MANCHE(
        ID_MANCHE Int  Auto_increment  NOT NULL ,
        POINT_J1  Int NOT NULL ,
	,CONSTRAINT MANCHE_PK PRIMARY KEY (ID_MANCHE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MATCH
#------------------------------------------------------------

CREATE TABLE MATCHS(
        ID_MATCH         Int  Auto_increment  NOT NULL ,
        DATE_HEURE_DEBUT Datetime NOT NULL ,
        DATE_HEURE_FIN   Datetime NOT NULL ,
        ID_TOURNOI       Int NOT NULL ,
        ID_ARBITRE       Int NOT NULL
	,CONSTRAINT MATCH_PK PRIMARY KEY (ID_MATCH)

	,CONSTRAINT MATCH_TOURNOI_FK FOREIGN KEY (ID_TOURNOI) REFERENCES TOURNOI(ID_TOURNOI)
	,CONSTRAINT MATCH_ARBITRE0_FK FOREIGN KEY (ID_ARBITRE) REFERENCES ARBITRE(ID_ARBITRE)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: comprends
#------------------------------------------------------------

CREATE TABLE comprends(
        ID_MATCH  Int NOT NULL ,
        ID_MANCHE Int NOT NULL ,
        ID_JOUEUR1 Int NOT NULL ,
        ID_JOUEUR2 Int NOT NULL,
        SERVICE   Bool NOT NULL
	,CONSTRAINT comprends_PK PRIMARY KEY (ID_MATCH,ID_MANCHE,ID_JOUEUR1,ID_JOUEUR2)

	,CONSTRAINT comprends_MATCH_FK FOREIGN KEY (ID_MATCH) REFERENCES MATCHS(ID_MATCH)
	,CONSTRAINT comprends_MANCHE0_FK FOREIGN KEY (ID_MANCHE) REFERENCES MANCHE(ID_MANCHE)
	,CONSTRAINT comprends_JOUEUR1_FK FOREIGN KEY (ID_JOUEUR1) REFERENCES JOUEUR(ID_JOUEUR)
	,CONSTRAINT comprends_JOUEUR2_FK FOREIGN KEY (ID_JOUEUR2) REFERENCES JOUEUR(ID_JOUEUR)
)ENGINE=InnoDB;



